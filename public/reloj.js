function actual() {
    fecha = new Date(); //Actualizar fecha.
    hora = fecha.getHours(); //hora actual
    minuto = fecha.getMinutes(); //minuto actual
    segundo = fecha.getSeconds(); //segundo actual

    if (hora < 10) { //dos cifras para la hora
        hora = "0" + hora;
    }

    if (minuto < 10) { //dos cifras para el minuto
        minuto = "0" + minuto;
    }

    if (segundo < 10) { //dos cifras para el segundo
        segundo = "0" + segundo;
    }
    //ver en el recuadro del reloj:
    mireloj = hora + " : " + minuto + " : " + segundo;
    return mireloj;
}

function actualizarReloj() { //función del temporizador
    mihora = actual(); //recoger hora actual
    mireloj = document.getElementById("reloj"); //buscar elemento reloj
    mireloj.innerHTML = mihora; //incluir hora en elemento

}

setInterval(actualizarReloj, 1000); //iniciar temporizador

// RELOJ 2

var myVar = setInterval(myTimer, 1000);

function myTimer() {
    var d = new Date();
    var t = d.toLocaleTimeString();
    document.getElementById("reloj2").innerHTML = t;
}

function stopInterval() {
    clearInterval(myVar);
}

var interval3 = setInterval(changeBgColor, 1500);

function changeBgColor() {
    var colors = [
        "aqua", "red", "yellow", "blue", "green", "orange", "grey", "skyblue"
    ];
    var randomColor = Math.floor(Math.random() * colors.length)
    document.body.style.backgroundColor = colors[randomColor];
}
